from setuptools import setup

setup(
    name="programaker-sheets-service",
    version="0.1",
    description="Programaker service to interact with a Google Sheets instance.",
    author="kenkeiras",
    author_email="kenkeiras@codigoparallevar.com",
    license="Apache License 2.0",
    packages=["programaker_gsheets_service"],
    scripts=["bin/programaker-gsheets-service"],
    include_package_data=True,
    install_requires=["google-auth-oauthlib", "programaker-bridge", "sqlalchemy"],
    zip_safe=False,
)
