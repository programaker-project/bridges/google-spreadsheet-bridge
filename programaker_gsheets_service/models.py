from sqlalchemy import (Column, ForeignKey, Integer, MetaData, String, Table,
                        Text, UniqueConstraint)

metadata = MetaData()

GsheetsUsers = Table(
    "GSHEETS_USER_REGISTRATION",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("gsheets_credentials", Text()),
)

PlazaUsers = Table(
    "PLAZA_USERS",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("plaza_user_id", String(36), unique=True),
)

PlazaUsersInGsheets = Table(
    "PLAZA_USERS_IN_GSHEETS",
    metadata,
    Column("plaza_id", Integer, ForeignKey("PLAZA_USERS.id"), primary_key=True),
    Column(
        "gsheets_id",
        Integer,
        ForeignKey("GSHEETS_USER_REGISTRATION.id"),
        primary_key=True,
    ),
    __table_args__=(UniqueConstraint("plaza_id", "gsheets_id")),
)
